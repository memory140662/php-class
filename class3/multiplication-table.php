<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>乘法表</title>
    <style>
        table th,
        table td {
            width: 20px;
        }

        table th {
            text-align: center;
        }

        table td {
            text-align: right;
        }
    </style>
</head>
<body>
    <?php
        $array = array(
            array(1,  2,  3,  4,  5,  6,  7,  8,  9),
            array(2,  4,  6,  8, 10, 12, 14, 16, 18),
            array(3,  6,  9, 12, 15, 18, 21, 24, 27),
            array(4,  8, 12, 16, 20, 24, 28, 32, 36),
            array(5, 10, 15, 20, 25, 30, 35, 40, 45),
            array(6, 12, 18, 24, 30, 36, 42, 48, 54),
            array(7, 14, 21, 28, 35, 42, 49, 56, 63),
            array(8, 16, 24, 32, 40, 48, 56, 64, 72),
            array(9, 18, 27, 36, 45, 54, 63, 72, 81),
        );
    ?>

    <table border='1'>
        <tr>
            <th></th>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
        </tr>
        <?php
            foreach($array as $value_array) {
                echo "<tr>";
                echo "  <th>$value_array[0]</th>";
                foreach($value_array as $value) {
                    echo "  <td>$value</td>";
                }
                echo "</tr>";
            }
        ?>
    </table>
</body>
</html>