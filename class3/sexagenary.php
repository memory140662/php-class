<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>天干地支</title>
</head>
<body>
    <?php

        $year = 2022;
        // 天干
        $celestial_stems = array('甲', '乙', '丙', '丁', '戊', '己', '庚', '辛', '壬', '癸');
        $celestial_stem_count = count($celestial_stems);
        // 地支
        $terrestrial_branches = array('子', '丑', '寅', '卯', '辰', '巳', '午', '未', '申', '酉', '戌', '亥');
        $terrestrial_branch_count = count($terrestrial_branches);
        
        const START_YEAR = 1024;
        $diff_year = $year - START_YEAR;

        $celestial_stem_diff_year = $diff_year % $celestial_stem_count;
        $terrestrial_branch_diff_year = $diff_year % $terrestrial_branch_count;

        $celestial_stem_result = "";
        $terrestrial_branch_result = "";
        // 計算年份大於等於1024的情況
        // 獲取天干結果
        for ($index = 0; $index <= $celestial_stem_diff_year ; $index++) {
            $celestial_stem_result = $celestial_stems[$index];
        }
        // 獲取地支結果
        for ($index = 0; $index <= $terrestrial_branch_diff_year ; $index++) {
            $terrestrial_branch_result = $terrestrial_branches[$index];
        }

        // 計算年份小於1024的情況
        // 獲取天干結果
        for ($index = -1; $index >= $celestial_stem_diff_year; $index--) {
            $celestial_stem_result = $celestial_stems[$celestial_stem_count + $index];
        }
        // 獲取地支結果
        for ($index = -1; $index >= $terrestrial_branch_diff_year; $index--) {
            $terrestrial_branch_result = $terrestrial_branches[$terrestrial_branch_count + $index];
        }

        echo $year . " 年為 " . $celestial_stem_result . $terrestrial_branch_result;
    ?>
</body>
</html>
