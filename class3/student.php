<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>學生成績</title>
</head>
<body>
    <?php 
        $courses = array(
            'chinese' => '國文',
            'english' => '英文',
            'math' => '數學',
            'geography' => '地理',
            'history' => '歷史',
        );

        $students = array(
            'Judy' => array(
                'chinese' => 95,
                'english' => 64,
                'math' => 70,
                'geography' => 90,
                'history' => 84,
            ),
            'Amo' => array(
                'chinese' => 88,
                'english' => 78,
                'math' => 54,
                'geography' => 81,
                'history' => 71,
            ),
            'John' => array(
                'chinese' => 45,
                'english' => 60,
                'math' => 68,
                'geography' => 70,
                'history' => 62,
            ),
            'Peter' => array(
                'chinese' => 59,
                'english' => 32,
                'math' => 77,
                'geography' => 54,
                'history' => 42,
            ),
            'Hebe' => array(
                'chinese' => 71,
                'english' => 62,
                'math' => 80,
                'geography' => 42,
                'history' => 62,
            )
        );
    ?>

    <table border='1'>
        <tr align='center'>
            <th>姓名</th>
            <?php
                foreach($courses as $course_chinese_name) {
                    echo "<th>$course_chinese_name</th>";
                }
            ?>
        </tr>
        <?php 
            foreach($students as $name => $scores) {
                echo "<tr align='center'>";
                echo "  <td align='left'>$name</td>";

                foreach(array_keys($courses) as $course_name) {
                    echo "<td>$scores[$course_name]</td>";
                }

                echo "</tr>";
            }
        ?>
    </table>
</body>
</html>