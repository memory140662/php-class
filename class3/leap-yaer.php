<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>近五百年的閏年</title>
    <style>
        .content {
            word-break: break-all;
        }
    </style>
</head>
<body>
    <div class='content'>
    <?php
        $current_year = 2022; // 當前年份
        $last_year = 500; // 最近幾年
        $years = array(); // 閏年的陣列
        
        for ($year = $current_year - $last_year; $year <= $current_year; $year++) {
            if ($year % 400 === 0 || ($year % 100 !== 0 && $year % 4 === 0)) {
                array_push($years, $year);
            }
        }

        echo "自" . $current_year . "年開始，最近" . $last_year . "年的閏年有：" . join(",", $years);
    ?>
    </div>
</body>
</html>