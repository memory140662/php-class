<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        * {
            font-family: monospace;
            font-size: 20px;
        }
    </style>
</head>
<body>
    <?php
        $level = 7;
        $center_point = (int) ($level / 2);
        
        for ($row = 0; $row < $level; $row++) {
            for ($col = 0; $col < $level; $col++) {
                // 方法一
                if ($row + $col >= (($level - 1) - $center_point)
                    && $row + $col <= (($level - 1) + $center_point)
                    && $row <= $col + $center_point
                    && $row >= $col - $center_point) {
                    echo "*";
                } else {
                    echo "&nbsp;";
                }

                // 方法二
                // if ($row + $col < (($level - 1) - $center_point)
                //     || $row + $col > (($level - 1) + $center_point)
                //     || $row > $col + $center_point
                //     || $row < $col - $center_point) {
                //     echo "&nbsp;";
                // } else {
                //     echo "*";
                // }
            }
            echo "<br/>";
        }
    ?>
</body>
</html>